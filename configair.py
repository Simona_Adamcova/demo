from selenium import webdriver
import time

driver = webdriver.Chrome(executable_path="C:\Drivers\chromedriver_win32\chromedriver.exe")

driver.get("https://capdemo.configair.com/config-ui/#/profile/DORMAKABA_EU/lang/de")

driver.maximize_window()
time.sleep(1)

#should click on BASISDATEN
driver.find_element_by_xpath("//*[@id='root']/div/div[2]/div[1]/div/div[1]/div[2]").click()
time.sleep(1)

#should click on input Zargentyp
driver.find_elements_by_css_selector("input.input")[0].click()
time.sleep(1)

#should choose Bandaufn. V 3600 (Holzz.)
driver.find_element_by_xpath("//*[@id='root']/div/div[2]/div[1]/div/div[2]/div[1]/div[2]/div/div/div[1]/div/div[2]/div/div").click()
time.sleep(1)

#should click on input Falztiefe
driver.find_elements_by_css_selector("input.input")[1].click()
time.sleep(2)

#should choose 24mm
driver.find_element_by_xpath("//*[@id='root']/div/div[2]/div[1]/div/div[2]/div[2]/div[2]/div/div/div[1]/div/div[2]/div").click()
time.sleep(2)

#should click on input Zargenfalzma� Breite [mm]
inputBreite = driver.find_elements_by_css_selector("input.input")[3]
inputBreite.click()
time.sleep(1)

#should writte 1000 in to input
inputBreite.send_keys ("1000")
time.sleep(1)

#should click on input Zargenfalzma� H�he [mm]
driver.find_elements_by_css_selector("input.input")[4].click()

#should writte 2000 in to input
driver.find_elements_by_css_selector("input.input")[4].send_keys ("2000")
time.sleep(1)

#selector "svg image" is the place where "https://capdemo.configair.com/images/WAND1.svg" is located
#by insuring that selector is displayed, we can prove that "WAND1.svg" is displaed on a page
imgSvg = driver.find_element_by_css_selector("svg image")

#should print true or false
print (imgSvg.is_displayed())

driver.quit()